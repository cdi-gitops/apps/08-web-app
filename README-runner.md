Gitlab Runner einrichten
========================

Kubernetes
----------

Der GitLab Runner kann mittels eines HELM Charts eingerichtet werden.

**Installation GitLab Runner** (falls nicht bereits erfolgt)

Dazu in microk8s zuerst HELM3 aktivieren. GitLab Repo hinzufügen und GitLab Runner installieren.

    microk8s enable helm3
    microk8s helm3 repo add gitlab https://charts.gitlab.io
    microk8s kubectl create namespace gitlab
    microk8s helm3 install -n gitlab gitlab-runner gitlab/gitlab-runner  

Die Original Anleitung findet man [hier](https://docs.gitlab.com/runner/install/kubernetes.html).

**GitLab Runner Registrieren**

Nach der Installation ist der GitLab Runner zu registrieren.

Den entsprechenden Token findet man im GitLab Repository, des Projektes, unter -> Settings -> CD/CD -> Runners

    export REGISTRATION_TOKEN=<Ctrl+p>
    microk8s helm3 upgrade -n gitlab gitlab-runner --set gitlabUrl=https://gitlab.com,runnerRegistrationToken=${REGISTRATION_TOKEN} gitlab/gitlab-runner

Im GitLab UI ist für den Runner der Tag `kubernetes` zu setzen. Damit für den CI/CD Job der registrierte Runner verwendet wird.

Docker
------

Für Docker braucht es eine Installierte Docker Umgebung. Zusätzlich muss der User `gitlab-runner` `docker`-Befehle ausführen können.

Installation Standard Pakete und Docker

    sudo apt-get -y update
    sudo apt-get install -y curl openssh-server docker.io

Erstellen User `gitlab-runner` und Installation Runner.

    sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
    sudo chmod +x /usr/local/bin/gitlab-runner
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
    sudo usermod -aG docker gitlab-runner 
    sudo usermod -aG docker ubuntu      
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    sudo gitlab-runner start

Zum Schluss muss der `gitlab-runner` mit GitLab verbunden werden:

    sudo gitlab-runner register --url http://<Gitlab-Instanz>/ --registration-token <token>

Die Ausgabe ist wie folgt:

    Runtime platform   arch=amd64 os=linux pid=19578 revision=d540b510 version=15.9.1
    Running in system-mode.

    Enter the GitLab instance URL (for example, https://gitlab.com/): [....]:
    Enter the registration token: [...]:
    Enter a description for the runner:
    [cicd-22-default]: docker-runner
    Enter tags for the runner (comma-separated): <leer lassen>
    Enter optional maintenance note for the runner:
    Registering runner... succeeded                     runner=itJ6mxiW
    Enter an executor: docker-ssh, parallels, shell, instance: docker
    Enter the default Docker image (for example, ruby:2.7): ubuntu:22.04
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"

**Zu Beachten**
* Als `executor` docker angeben
* Keine Tags angeben. Damit können für einzelne Repositories gezielt eigene Runner angegeben werden.

Shell
-----

Auch bei einer Shell Umgebung wird immer ein Container Image gestartet. Default wird das Container Image der Eingabe (siehe oben bei Docker) verwendet.

Die restliche Installation entspricht der Docker Installation oben.

Schlägt der Jobs fehlt ist die Datei `.bash_logout` im HOME Verzeichnis des User `gitlab-runner` wegzulöschen.

